const axios = require('axios')


const getLugar = async (direccion) => {

    let encodedURL = encodeURI(direccion)

    let resp = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${encodedURL}&key=AIzaSyDyJPPlnIMOLp20Ef1LlTong8rYdTnaTXM`)

    if (resp.data.status === 'ZERO_RESULTS')
      throw new Error(`No se encontró la dirección ${direccion}`)
  
    let lugar = resp.data.results[0].formatted_address
    let coords = resp.data.results[0].geometry.location

    return {
      lugar,
      lat: coords.lat,
      lng: coords.lng
    }
  }

module.exports = {
  getLugar
}