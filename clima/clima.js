const axios = require('axios')


const getClima = async (lat,lng) => {

  let resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&appid=7b7387b5b052bf302516a3fa5163acc2&units=metric`)
  if(resp.data.cod === '400')
    throw Error('Coordenadas no validas:',lat,lng)
  return {temp: resp.data.main.temp,
    temp_min: resp.data.main.temp_min,
    temp_max : resp.data.main.temp_max
  }
}

module.exports = {
  getClima
}
