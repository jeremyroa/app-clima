###Aplicación Clima Actual

Sencilla aplicación que permite observar el clima actual, de alguna ciudad. Mostrada por consola

Cómo instalar:

```
npm install
```

Ayuda
```
node app --help
```

Ejemplo
```
node app -d "Merida Venezuela"
```