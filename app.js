const argv = require('./config/yargs').argv
const getLugar = require('./lugar/lugar').getLugar
const clima = require('./clima/clima')
const colors = require('colors')

const climaActual = async (direccion) => {
  
  try{
    let lugar = await getLugar(direccion)
    
    let temperatura = await clima.getClima(lugar.lat,lugar.lng)

    return `El clima en: ${colors.inverse(lugar.lugar)}\nLa temperatura actual es: ${colors.bold(temperatura.temp)}\nLa temperatura min es: ${colors.italic(temperatura.temp_min)}\nLa temperatura max es: ${colors.red(temperatura.temp)}\nSus coordenadas son: Latitud ${lugar.lat} Longitud ${lugar.lng}`
  }catch(err){
    return `No se pudo determinar el clima en ${direccion}`
  }
}
climaActual(argv.direccion).then(resp => console.log(resp)).catch(err => console.log(err))